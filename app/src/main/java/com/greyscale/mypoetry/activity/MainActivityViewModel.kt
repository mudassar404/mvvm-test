package com.greyscale.mypoetry.activity

import android.app.Application
import android.app.Person
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.greyscale.mypoetry.model.datamodels.person.Perosn
import com.greyscale.mypoetry.model.datasource.local.database.AppDatabase
import com.greyscale.mypoetry.repository.MainActivityRepository

class MainActivityViewModel (application: Application): AndroidViewModel(application) {
    private  var mainActivityRepository  : MainActivityRepository
    init {
        var personDao = AppDatabase.getAppDataBase(application)!!.personDao()
        mainActivityRepository = MainActivityRepository(personDao)
    }

    fun getDataFromApi() : LiveData<List<Perosn>> {
        return mainActivityRepository.getDataFromApi()
    }

    fun getDatafromDbId(id:Int): LiveData<Perosn> {
        return  mainActivityRepository.getDatafromDbById(id)
    }
}