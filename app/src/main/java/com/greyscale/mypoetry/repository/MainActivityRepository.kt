package com.greyscale.mypoetry.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.greyscale.mypoetry.model.api.WebService
import com.greyscale.mypoetry.model.datamodels.person.Perosn
import com.greyscale.mypoetry.model.datasource.local.dao.PersonDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityRepository (private var personDao: PersonDao){

    private var apiInterface= WebService.retrofitService.getData()
    private  var personList =  MutableLiveData<List<Perosn>>()
////////////all data method//////
    fun getDataFromApi() : LiveData<List<Perosn>> {
        apiInterface.enqueue(object : Callback<List<Perosn>> {
            override fun onResponse(call: Call<List<Perosn>>?, response: Response<List<Perosn>>?) {
                if (response!!.isSuccessful) {
                    Log.d("APIRequest", "onResponse: ${response.body()}")
                    personList.postValue(response.body())
                    for (person in response.body()!!){
                        //dispatcher of couritne to work in bakgorund thread as simple coutine doent means runing in abckhrgun ti stillw orks in main thread
                        CoroutineScope(Dispatchers.IO).launch {
                            personDao.insertPerson(person )
                        }
                    }
                    //response.body()?.let { recylerAdapter.setPersonListItem(it) }
                }
            }
            override fun onFailure(call: Call<List<Perosn>>?, t: Throwable?) {
                Log.d("APIRequest", "onFailure: ${t!!.message}")
            }
        })
        return personDao.getAllPerson()
    }


    ////////////all data method//////

    fun getDatafromDbById(id:Int):LiveData<Perosn>{
        return  personDao.getpersonById(id)
    }

}