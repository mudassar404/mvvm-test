package com.greyscale.mypoetry.model.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.logging.HttpLoggingInterceptor
object RetroFitClient {
    private var mInstance: Retrofit? = null
    var BASE_URL = "https://jsonplaceholder.typicode.com/"

    private val okHttpClient=OkHttpClient.Builder().addInterceptor{
            chain -> val original = chain.request()

        val requestBuilder = original.newBuilder().header("Authorization", "")
        // .method(original.method, original.body)
        val request = requestBuilder.build()
        chain.proceed(request)
    }

    
    fun getInstance(): Retrofit? {
        okHttpClient.callTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        okHttpClient.addInterceptor(logging)
        if (mInstance == null) {
            mInstance = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return mInstance
    }


}