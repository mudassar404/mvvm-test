package com.greyscale.mypoetry.model.datamodels.person

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "data_table")
data class Perosn(
    @NotNull
    @PrimaryKey
    val id: Int,
    val title: String,
    val body: String
) {

}