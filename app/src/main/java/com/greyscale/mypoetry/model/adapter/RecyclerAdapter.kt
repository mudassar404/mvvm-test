package com.greyscale.mypoetry.model.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.greyscale.mypoetry.R
import com.greyscale.mypoetry.model.datamodels.person.Perosn

class RecyclerAdapter(val context:Context) : RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {

    var PersonList: List<Perosn> = listOf()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cardview, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvId.text = PersonList.get(position).id.toString()
        holder.tvTitle.text = PersonList.get(position).title
        holder.tvBody.text = PersonList.get(position).body

    }

    override fun getItemCount(): Int {
       return  PersonList.size
    }

    fun setPersonListItem(movieList: List<Perosn>) {
        this.PersonList = movieList;
        notifyDataSetChanged()
    }
    /////////////View Holder Class
    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        val tvId: TextView = itemView!!.findViewById(R.id.person_id_cardView)
        val tvTitle: TextView = itemView!!.findViewById(R.id.person_title_cardView)
        val tvBody: TextView = itemView!!.findViewById(R.id.person_body_cardView)
    }



}