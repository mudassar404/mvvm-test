package com.greyscale.mypoetry.model.api

object WebService {

    val retrofitService : WebInterfaceAPi by lazy{
              RetroFitClient.getInstance()!!.create(WebInterfaceAPi::class.java)
    }

}