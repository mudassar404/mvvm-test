package com.greyscale.mypoetry.model.datasource.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.greyscale.mypoetry.model.datamodels.person.Perosn

@Dao
interface PersonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPerson(person: Perosn)


    @Query("SELECT *FROM data_table")
    fun getAllPerson() : LiveData<List<Perosn>>

    @Query("Select *from data_table where id = :id")
    fun getpersonById(id: Int): LiveData<Perosn>



}